public class AsciiText {
	public static void main(String args[]) {
	// This is a simple program to display basic Ascii text using java
	// Ascii 39,92 are also Displayed
	char[] str={ ' ','!','"', '#','$','%','&',
			'\'',  // 39 Ascii
			'(',')','*','+',',','-','.','/',
			'0','1','2','3','4','5','6','7','8','9',':',';',
			'<','=','>','?','@','A','B','C','D','E','F','G','H','I','J','K',
			'L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
			'[',
			'\\', // 92 Ascii
			']','^','_','`', 
			'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r',
			's','t','u','v','w','x','y','z','{','|','}','~'
			};
		for(int i=0;i<str.length;i++) {
			System.out.println("Ascii at "+(i+32) +" = " +str[i]);
		}	
}
}
